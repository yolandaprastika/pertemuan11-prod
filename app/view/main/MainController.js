/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Pertemuan11.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        Ext.getStore('detailpersonnel').getProxy().setExtraParams({
            user_id: record.data.user_id
        });
        Ext.getStore('detailpersonnel').load();
    },

    onBarItemSelected:function (sender, record) {
        //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
        Ext.getStore('detailpersonnel').getProxy().setExtraParams({
            user_id: record.data.user_id
        });
        Ext.getStore('detailpersonnel').load();
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onReadDetailClicked: function(){
        Ext.getStore('detailpersonnel').load();
    },

    onReadClicked: function(){
        Ext.getStore('personnel').load();
    },

    onSimpanPerubahan: function(){
       name = Ext.getCmp('myname').getValue();
       email = Ext.getCmp('myemail').getValue();
       phone = Ext.getCmp('myphone').getValue();

        store = Ext.getStore('personnel');
        record = Ext.getCmp('mydataview').getSelection();
        index = store.indexOf(record);
        record = store.getAt(index);
        store.beginUpdate();
        record.set('name',name);
        record.set('email',email);
        record.set('phone', phone);
        store.endUpdate();
        alert("Updating..");
    },
    onTambahPersonnel: function(){
       name = Ext.getCmp('myname').getValue();
       email = Ext.getCmp('myemail').getValue();
       phone = Ext.getCmp('myphone').getValue();

        store = Ext.getStore('personnel');
        store.beginUpdate(); //bukan hanya u/ update,tp juga untuk insert 
        store.insert(0, {'name':name,'email':email,'phone': phone });
        store.endUpdate();
        alert("Inserting..");
    }
});

function onDeletePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    Ext.getStore('personnel').remove(record);
    alert(user_id);
};

// function onUpdatePersonnel(user_id){
//     store = Ext.getStore('personnel');
//     record = Ext.getCmp('mydataview').getSelection();
//     index = store.indexoOf(record);
//     record = store.getAt(index);
//     store.beginUpdate();
//     record.set('name',"Atmala Sari Harahap");
//     record.set('email',"atmala@student.uir.ac.id");
//     record.set('phone', "085156696308");
//     store.endUpdate();
//     alert("Updating..");
// };

function onUpdatePersonnel(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    name = record.data.name;
    email = record.data.email;
    phone = record.data.phone;
    Ext.getCmp('myname').setValue(name);
    Ext.getCmp('myemail').setValue(email);
    Ext.getCmp('myphone').setValue(phone);
};



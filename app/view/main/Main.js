/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan11.view.main.Main', {
   extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan11.view.main.MainController',
        'Pertemuan11.view.main.MainModel',
        'Pertemuan11.view.main.List',
        'Pertemuan11.view.main.FormPanel',
        // 'Pertemuan11.view.main.Bar',
        // 'Pertemuan11.store.Bar',
        // 'Pertemuan11.view.main.Bar',
        // 'Pertemuan11.view.main.BarList',
        'Pertemuan11.store.DetailPersonnel'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            docked: 'top',
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Read',
                ui: 'action',
                scope: this,
                listeners: {
                    tap: 'onReadClicked'
                }
            }]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items:[{
                    xtype: 'mainlist',
                    flex: 2
                },{
                    xtype: 'detail',
                    flex: 1
                },
                {
                    xtype: 'editform',
                    flex: 1
                }],
            }]
        },{
            title: 'Charts',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
             bind: {
                html: '{loremIpsum}'
            }
            // items: [{
            //     xtype: 'panel',
            //     layout: 'vbox',
            //     items:[{
            //         xtype: 'bar',
            //         flex: 1
            //     },{
            //         xtype: 'barlist',
            //         flex: 1
            //     }],
            // }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            bind: {
                html: '{loremIpsum}'
            }
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});

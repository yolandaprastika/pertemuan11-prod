/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan11.view.main.List', {
   extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan11.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],

    plugins:[{
        type: 'grideditable'
    }],

    title: 'Data Diri Mahasiswa',


    bind: '{personnel}',

    viewModel:{
        stores:{
            personnel:{
                type:'personnel'
            }
        }
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 230, editable: true },
        { text: 'Email', dataIndex: 'email', width: 230, editable: true },
        { text: 'Phone', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});

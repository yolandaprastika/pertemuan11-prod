Ext.define('Pertemuan11.view.main.FormPanel', {
    extend: 'Ext.form.Panel',
    shadow: true,
    xtype:'editform',
    id: 'editform',
    items: [
        {
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    id:'myname',
                    label: 'Name',
                    placeHolder: 'Your Name',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    id:'myemail',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'phone',
                    id:'myphone',
                    label: 'Phone',
                    placeHolder: '021**********',
                    clearIcon: true
                },
                {
                    xtype:'button',
                    text: 'Simpan',
                    ui: 'action',
                    handler: 'onSimpanPerubahan'
                },
                {
                    xtype:'button',
                    text: 'Tambah Data',
                    ui: 'Confirm',
                    handler: 'onTambahPersonnel'
                }
            ]
        }
    ]
});